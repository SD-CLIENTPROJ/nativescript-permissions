import { Common } from './permissions.common';
import { PermissionsConstructor as IOSPermissions } from './permissions.ios';
import { PermissionsConstructor as ANDROIDPermissions } from './permissions.android'; 

export const Permissions : IOSPermissions | ANDROIDPermissions;