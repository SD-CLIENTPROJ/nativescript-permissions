import { Observable } from 'tns-core-modules/data/observable';
import * as app from 'tns-core-modules/application';
import * as dialogs from 'tns-core-modules/ui/dialogs';

export class Common extends Observable {

  public status: { [key: string]: any } = {};

  public log(...messages: any[]) {
    console.log(['[@SpartaDigital/nativescript-permissions] ', ...messages].join(' '));
  }

}

