import { AndroidApplication, android as AndroidApp, AndroidActivityRequestPermissionsEventData } from 'tns-core-modules/application';
import {  } from 'tns-core-modules/platform//platform';

import { Common } from './permissions.common';

import { hasPermission, requestPermissions } from 'nativescript-permissions';

// declare var android: any;

export const Intent = android.content.Intent;
export const URI = android.net.Uri;

export function getContext() {
	//noinspection JSUnresolvedVariable,JSUnresolvedFunction
	var ctx = java.lang.Class.forName("android.app.AppGlobals").getMethod("getInitialApplication", null).invoke(null, null);
	if (ctx) { return ctx; }

	//noinspection JSUnresolvedVariable,JSUnresolvedFunction
	return java.lang.Class.forName("android.app.ActivityThread").getMethod("currentApplication", null).invoke(null, null);
}

export class PermissionsConstructor extends Common {

    private CameraPermissions = [(android as any).Manifest.permission.CAMERA];
    private LocationPermissions = [(android as any).Manifest.permission.ACCESS_FINE_LOCATION ];
    private FilePermissions = [(android as any).Manifest.permission.WRITE_EXTERNAL_STORAGE, (android as any).Manifest.permission.READ_EXTERNAL_STORAGE];

    private pendingPromises = {};
    private promiseId: number = 3000;

    constructor() {
        super();
    }

    public openAppSettings() {
        const currentActivity = AndroidApp.foregroundActivity;
        const packageUrl = URI.fromParts('package', AndroidApp.packageName, null);
        const intent = new Intent();
        intent.setAction(android.provider.Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        intent.setData(packageUrl);
        intent.addCategory(Intent.CATEGORY_DEFAULT);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
      
        currentActivity.startActivity(intent);        
    }

    public hasPermission(permission: any[]): boolean;
    public hasPermission(permission: any): boolean {
        if ( !Array.isArray(permission) ) {
            permission = [ permission ];
        }
        return hasPermission([...permission]);
    }

    public requestPermissions(permissions: any[], explanation: any): Promise<any>;
    public requestPermissions(permissions: any, explanation: any = ''): Promise<any> {
        if ( !Array.isArray(permissions) ) {
            permissions = [ permissions ];
        }

        return requestPermissions([ ...permissions ], explanation);
    }

    public hasCameraPermission(): boolean {
        return hasPermission((android as any).Manifest.permission.CAMERA);
    }

    public requestCameraPermission(): Promise<boolean> {
        return requestPermissions(this.CameraPermissions)
                .then((args) => {
                    this.log('#requestCameraPermission success');
                    this.log(`-- Has Args? ${!!args}`);
                    this.log(`-- ${JSON.stringify(args, null, 4)}`);
                    return true;
                },
                (args) => {
                    this.log('#requestCameraPermission failed');
                    this.log(`-- Has Args? ${!!args}`);
                    this.log(`-- ${JSON.stringify(args, null, 4)}`);
                    return false;
                });
    }

    public hasLocationPermission(): boolean {
        return hasPermission((android as any).Manifest.permission.ACCESS_FINE_LOCATION);
    }

    public requestLocationPermission(): Promise<boolean> {
        return requestPermissions(this.LocationPermissions)
                .then((args) => {
                    this.log('#requestLocationPermission success');
                    this.log('-- Has Args? ', !!args);
                    this.log(`-- ${JSON.stringify(args, null, 4)}`);
                    return true
                },
                (args) => {
                    this.log('#requestLocationPermission failed');
                    this.log('-- Has Args? ', !!args);
                    this.log(`-- ${JSON.stringify(args, null, 4)}`);
                    return false
                });
    }

    public hasFilePermission(): boolean {
        let hasFilePermissions: boolean = false;

        this.FilePermissions.forEach((val) => {
            hasFilePermissions = true === (hasFilePermissions && hasPermission(val));
        });

        return hasFilePermissions;
    }

    public requestFilePermission(): Promise<boolean> {
        return requestPermissions(this.FilePermissions)
                .then((args: PermissionResults) => {
                    this.log('#requestFilePermission success');
                    this.log('-- Has Args? ', !!args);
                    this.log(`-- ${JSON.stringify(args, null, 4)}`);
                    return true;
                },
                (args: PermissionResults) => {
                    this.log('#requestFilePermission failed');
                    this.log('-- Has Args? ', !!args);
                    this.log(`-- ${JSON.stringify(args, null, 4)}`);
                    return false;
                });
    }

}

class PermissionResults {
    [PermissionName: string]: boolean;
}

export const Permissions = new PermissionsConstructor();