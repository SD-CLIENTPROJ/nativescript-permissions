import { Common } from './permissions.common';
import * as platform from 'tns-core-modules/platform';
import { openUrl } from 'tns-core-modules/utils/utils';

import { isString, isFinite, find } from 'lodash';

declare const UIBackgroundRefreshStatus: any;
declare const ABAuthorizationStatus: any;
declare const AVAuthorizationStatus: any;
//declare const PHAuthorizationStatus: any;
declare const CLAuthorizationStatus: any;
declare const AVAudioSessionRecordPermission: any;
declare const EKAuthorizationStatus: any;

var PERMISSION_STATUS = {
    0: 'Not Determined',
    1: 'Restricted',
    2: 'Denied',
    3: 'Authorized Always',
    4: 'Authorized WhenInUse'
}

export class PermissionsConstructor extends Common {


    constructor() {
        super();

        this.mapStatus('BackgroundRefresh', UIBackgroundRefreshStatus);
        this.mapStatus('Contacts', ABAuthorizationStatus);
        this.mapStatus('Camera', AVAuthorizationStatus);
        //this.mapStatus('Pictures', PHAuthorizationStatus);
        this.mapStatus('Location', CLAuthorizationStatus);
        this.mapStatus('Microphone', AVAudioSessionRecordPermission);
        this.mapStatus('Calendar', EKAuthorizationStatus);
    }


    private mapStatus(key: string, obj: any) {
        this.status[key] = {};
        const statuses = [
            'NotDetermined',
            'Restricted',
            'Denied',
            'Authorized',
            'Available',
            'AuthorizedAlways',
            'AuthorizedWhenInUse',
            'Undetermined',
            'Granted'
        ];

        for (let i; i < statuses.length; i++) {
            let status = find(obj, (o, key) => {
                return (isString(key) && key.indexOf(statuses[i]) !== -1) ? true : false;
            });

            if (isFinite(status)) {
                this.status[key][statuses[i]] = obj[obj[status]];
                this.status[key][obj[obj[status]]] = statuses[i];
            }
        }
    }


    /**
     * Location Permissions!
     */


    private _locationListener: LocationListener;
    private _locationManager: CLLocationManager;

    public openAppSettings(): void {
        openUrl(UIApplicationOpenSettingsURLString);
    }

    public isLocationEnabled(): boolean {
        return CLLocationManager.locationServicesEnabled();
    }

    public getLocationAuthorizationStatus(): number {
        return CLLocationManager.authorizationStatus();
    }

    public isLocationAuthorized(): boolean {
        let status: number = this.getLocationAuthorizationStatus();
        return (
            status === this.status['Location']['AuthorizedAlways']
            ||
            status === this.status['Location']['AuthorizedWhenInUse']
        );
    }

    public hasLocationPermission(): boolean {
        return this.isLocationEnabled() && this.isLocationAuthorized();
    }

    public requestLocationPermission(type: 'WhenInUse' | 'Always' = 'WhenInUse'): Promise<boolean> {
        let status: number = this.getLocationAuthorizationStatus();

        this.log(`status : ${status}`);
        this.log(`Status is NotDetermined ? ${status === 0}`);


        if (status !== 0) {
            this.log(`Resolving as true as location is ${PERMISSION_STATUS[status]}`);
            return Promise.resolve(true);
        }
 
        if (!this._locationListener) {
            this._locationListener = new LocationListener();
        }

        if (!this._locationManager) {
            this._locationManager = new CLLocationManager();
            this._locationManager.delegate = this._locationListener;
        }

        if (type === 'WhenInUse') {
            this._locationManager.requestWhenInUseAuthorization();
        } else {
            this._locationManager.requestAlwaysAuthorization();
        }

        return new Promise((resolve, reject) => {
            this._locationListener.setupPromise(resolve);
        });
    }

    /**
     * CAMERA PERMISSIONS
     */

    public isCameraPresent(): boolean {
        return UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera);
    }

    public isFrontCameraPresent(): boolean {
        return UIImagePickerController.isCameraDeviceAvailable(UIImagePickerControllerCameraDevice.Front);
    }

    public isRearCameraPresent(): boolean {
        return UIImagePickerController.isCameraDeviceAvailable(UIImagePickerControllerCameraDevice.Rear);
    }

    public getCameraAuthorizationStatus(): number {
        return AVCaptureDevice.authorizationStatusForMediaType(AVMediaTypeVideo);
    }

    public isCameraAuthorized(): boolean {
        return this.getCameraAuthorizationStatus() === this.status['Camera']['Authorized'];
    }

    public hasCameraPermission(): boolean {
        return this.isCameraPresent() && this.isCameraAuthorized();
    }

    public requestCameraPermission(): Promise<boolean> {
        return new Promise((resolve) => {
            AVCaptureDevice.requestAccessForMediaTypeCompletionHandler(AVMediaTypeVideo, resolve);
        });
    }

    /**
     * Pictures/Camera/"Files" Permissions
     */

    public getPicturesAuthorizationStatus(): number {
        return PHPhotoLibrary.authorizationStatus();
    }

    public hasFilePermission(): boolean {
        return this.getPicturesAuthorizationStatus() === PHAuthorizationStatus.Authorized;
    }

    public requestFilePermission(): Promise<boolean> {
        return new Promise((resolve) => {
            PHPhotoLibrary.requestAuthorization((status) => resolve(status === PHAuthorizationStatus.Authorized));
        });
    }

}

export class LocationListener extends NSObject implements CLLocationManagerDelegate {

    public static ObjCProtocols = [CLLocationManagerDelegate];
    private _resolves: any = [];
    private _didSetup: boolean;

    public locationManagerDidChangeAuthorizationStatus(manager: any, status: number) {
        if (!this._didSetup) {
            this._didSetup = true;
            return;
        }
        const len = this._resolves.length;

        for (let i = 0; i < len; i++) {
            this._resolves[i](status);
        }
    }

    public setupPromise(resolve: () => void) {
        if (this._resolves === undefined) {
            this._resolves = [];
        }
        this._resolves.push(resolve);
    }

}

export const Permissions: PermissionsConstructor = new PermissionsConstructor();